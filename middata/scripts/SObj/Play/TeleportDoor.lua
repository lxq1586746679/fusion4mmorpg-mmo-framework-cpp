local t = {tpPlayers={}}

t.events = {
    ObjectHookEvent.OnSObjUnitEnter,
    ObjectHookEvent.OnSObjUnitLeave,
}

function main(pSObj, args)
    t.obj = pSObj
    t.init(pSObj)
end

function t.init(pSObj)
    local proto = pSObj:GetProto()
    t.tpId = proto.teleportPointID
    t.tpDealyTime = proto.teleportDelayTime
    pSObj:AttachObjectHookInfo(t)
end

function t.OnSObjUnitEnter(pUnit)
    t.tpPlayers[pUnit:GetGuid()] = CreateHandlerTimer(t.obj,
        function() t.Run(pUnit) end, t.tpDealyTime, 1)
end

function t.OnSObjUnitLeave(pUnit)
    local handler = t.tpPlayers[pUnit:GetGuid()]
    if handler then
        RemoveTimer(t.obj, handler)
        t.tpPlayers[pUnit:GetGuid()] = nil
    end
end

function t.Run(pPlayer)
    t.tpPlayers[pPlayer:GetGuid()] = nil
    pPlayer:TeleportBy(t.tpId)
end