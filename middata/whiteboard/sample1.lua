cfg = {
	evaluate = function(obj, vars)
		if obj:GetHPRate() > 0.5 then
			return 1
		else
			return 2
		end
	end,
	combat = {
		[1] = {
			transitions = {
				{8,1,7},
			},
			spells = {
				{1,1,100,0,0,0},
				{2,1,5,3,0,5},
				{4,1,1,3,30000,0},
			},
			links = {
				{4,1,5,1,6,1},
			},
		},
		[2] = {
			transitions = {
				{9,1,7},
			},
			spells = {
				{1,1,100,0,0,0},
				{2,1,5,3,0,5},
			},
		},
	},
};