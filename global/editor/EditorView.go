package main

import (
	"test.com/worlddb"
)

type webSelectOption struct {
	Label string `json:"label"`
	Value int    `json:"value"`
}

type webCascaderOption struct {
	Value    int                 `json:"value"`
	Label    string              `json:"label"`
	Children []webCascaderOption `json:"children,omitempty"`
}

func getViewSpellResumeType() []webSelectOption {
	return []webSelectOption{
		{"离线移除", worlddb.SpellResumeType_RemoveWhenOffline},
		{"在线计时", worlddb.SpellResumeType_ContinueWhenOnline},
		{"按时间计时", worlddb.SpellResumeType_ContinueByTime},
	}
}

func getViewSpellTargetType() []webSelectOption {
	return []webSelectOption{
		{"任意", worlddb.SpellTargetType_Any},
		{"友方", worlddb.SpellTargetType_Friend},
		{"敌方", worlddb.SpellTargetType_Enemy},
		{"队友", worlddb.SpellTargetType_TeamMember},
	}
}

func getViewSpellPassiveModeCascade() []webCascaderOption {
	return []webCascaderOption{
		{worlddb.SpellPassiveMode_Status, "状态",
			[]webCascaderOption{
				{worlddb.SpellPassiveBy_StatusNone, "任意", nil},
				{worlddb.SpellPassiveBy_StatusHPValue, "血量数值", nil},
				{worlddb.SpellPassiveBy_StatusHPRate, "血量比例", nil},
			},
		},
		{worlddb.SpellPassiveMode_Event, "时机",
			[]webCascaderOption{
				{worlddb.SpellPassiveBy_EventHit, "击中", nil},
				{worlddb.SpellPassiveBy_EventHitBy, "被击中", nil},
			},
		},
	}
}

func getViewSpellInterruptBy() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.SpellInterruptBy_None},
		{"移动", worlddb.SpellInterruptBy_Move},
		{"受伤", worlddb.SpellInterruptBy_Injured},
	}
}

func getViewSpellEffectType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.SpellEffectType_None},
		{"攻击", worlddb.SpellEffectType_Attack},
		{"光环攻击", worlddb.SpellEffectType_AuraAttack},
		{"改变属性", worlddb.SpellEffectType_ChangeProp},
		{"改变速度", worlddb.SpellEffectType_ChangeSpeed},
		{"无敌", worlddb.SpellEffectType_Invincible},
		{"隐身", worlddb.SpellEffectType_Invisible},
		{"束缚", worlddb.SpellEffectType_Fetter},
		{"眩晕", worlddb.SpellEffectType_Stun},
		{"光环对象", worlddb.SpellEffectType_AuraObject},
		{"传送", worlddb.SpellEffectType_Teleport},
		{"采集", worlddb.SpellEffectType_Mine},
	}
}

func getViewSpellStageType() []webSelectOption {
	return []webSelectOption{
		{"吟唱", worlddb.SpellStageType_Chant},
		{"前摇", worlddb.SpellStageType_Channel},
		{"后摇", worlddb.SpellStageType_Cleanup},
	}
}

func getViewSpellSelectType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.SpellSelectType_None},
		{"自身", worlddb.SpellSelectType_Self},
		{"单个友方", worlddb.SpellSelectType_Friend},
		{"单个敌方", worlddb.SpellSelectType_Enemy},
		{"范围内友方", worlddb.SpellSelectType_Friends},
		{"范围内敌方", worlddb.SpellSelectType_Enemies},
		{"自身或单个友方", worlddb.SpellSelectType_SelfOrFriend},
		{"选择范围内友方", worlddb.SpellSelectType_FocusFriends},
		{"选择范围内敌方", worlddb.SpellSelectType_FocusEnemies},
		{"单个队友", worlddb.SpellSelectType_TeamMember},
		{"范围内队友", worlddb.SpellSelectType_TeamMembers},
		{"自身或单个队友", worlddb.SpellSelectType_SelfOrTeamMember},
		{"选择范围内队友", worlddb.SpellSelectType_FocusTeamMembers},
		{"引用", worlddb.SpellSelectType_Reference},
	}
}

func getViewSpellSelectMode() []webSelectOption {
	return []webSelectOption{
		{"圆形", worlddb.SpellSelectMode_Circle},
		{"扇形", worlddb.SpellSelectMode_Sector},
		{"矩形", worlddb.SpellSelectMode_Rect},
	}
}

func getViewSpellSelectModeArgs() []webSelectOption {
	return []webSelectOption{
		{"radius", worlddb.SpellSelectMode_Circle},
		{"radius,angle,offset,hole", worlddb.SpellSelectMode_Sector},
		{"length,width,offset", worlddb.SpellSelectMode_Rect},
	}
}

func getViewSpellEffectStyle() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.SpellEffectStyle_None},
		{"正BUFF", worlddb.SpellEffectStyle_Positive},
		{"负BUFF", worlddb.SpellEffectStyle_Negative},
	}
}

func getViewAuraSelectType() []webSelectOption {
	return []webSelectOption{
		{"友方", worlddb.AuraSelectType_Friend},
		{"敌方", worlddb.AuraSelectType_Enemy},
		{"玩家", worlddb.AuraSelectType_Player},
		{"怪物", worlddb.AuraSelectType_Creature},
		{"队友", worlddb.AuraSelectType_TeamMember},
	}
}

func getViewQuestClassType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.QuestClassType_None},
		{"主线", worlddb.QuestClassType_MainLine},
		{"支线", worlddb.QuestClassType_BranchLine},
	}
}

func getViewQuestObjType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.QuestObjType_None},
		{"向导点", worlddb.QuestObjType_Guide},
		{"NPC实例", worlddb.QuestObjType_NPC},
		{"静物实例", worlddb.QuestObjType_SObj},
		{"NPC原型", worlddb.QuestObjType_NPCPt},
		{"静物原型", worlddb.QuestObjType_SObjPt},
	}
}

func getViewQuestRepeatType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.QuestRepeatType_None},
		{"每天?次", worlddb.QuestRepeatType_Daily},
		{"每周?次", worlddb.QuestRepeatType_Weekly},
		{"每月?次", worlddb.QuestRepeatType_Monthly},
		{"限制?次", worlddb.QuestRepeatType_Forever},
	}
}

func getViewQuestConditionType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.QuestConditionType_None},
		{"对话", worlddb.QuestConditionType_TalkNPC},
		{"杀怪", worlddb.QuestConditionType_KillCreature},
		{"拥有数值", worlddb.QuestConditionType_HaveCheque},
		{"拥有物品", worlddb.QuestConditionType_HaveItem},
		{"使用物品", worlddb.QuestConditionType_UseItem},
		{"故事模式", worlddb.QuestConditionType_PlayStory},
	}
}

func getViewCharRaceType() []webSelectOption {
	return []webSelectOption{
		{"玩家", worlddb.CharRaceType_Player},
		{"中立", worlddb.CharRaceType_Neutral},
		{"敌方", worlddb.CharRaceType_HostileForces},
		{"友方", worlddb.CharRaceType_FriendlyForces},
	}
}

func getViewCharEliteType() []webSelectOption {
	return []webSelectOption{
		{"标准", worlddb.CharEliteType_Normal},
		{"BOSS", worlddb.CharEliteType_Boss},
	}
}

func getViewItemClassCascade() []webCascaderOption {
	return []webCascaderOption{
		{worlddb.ItemClass_Equip, "装备",
			[]webCascaderOption{
				{worlddb.ItemSubClass_Equip_Weapon, "武器", nil},
				{worlddb.ItemSubClass_Equip_Belt, "腰带", nil},
				{worlddb.ItemSubClass_Equip_Gloves, "手套", nil},
				{worlddb.ItemSubClass_Equip_Shoes, "鞋子", nil},
			},
		},
		{worlddb.ItemClass_Quest, "任务", nil},
		{worlddb.ItemClass_Material, "材料", nil},
		{worlddb.ItemClass_Gemstone, "宝石", nil},
		{worlddb.ItemClass_Consumable, "消耗品", nil},
		{worlddb.ItemClass_Scrap, "废品", nil},
		{worlddb.ItemClass_Other, "其它", nil},
	}
}

func getViewItemQuality() []webSelectOption {
	return []webSelectOption{
		{"白", worlddb.ItemQuality_White},
		{"红", worlddb.ItemQuality_Red},
	}
}

func getViewMapType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.MapInfo_Type_VoidSpace},
		{"野外地图", worlddb.MapInfo_Type_WorldMap},
		{"故事副本", worlddb.MapInfo_Type_Story},
		{"BOSS副本", worlddb.MapInfo_Type_PlayBoss},
		{"帮派联赛", worlddb.MapInfo_Type_GuildLeague},
	}
}

func getViewAttrArithType() []webSelectOption {
	return []webSelectOption{
		{"基础值", worlddb.ATTRARITHTYPE_BASE},
		{"缩放比率", worlddb.ATTRARITHTYPE_SCALE},
		{"最终加成", worlddb.ATTRARITHTYPE_FINAL},
	}
}

func getViewAttrType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.ATTRTYPE_NONE},
		{"生命", worlddb.ATTRTYPE_HIT_POINT},
		{"魔法", worlddb.ATTRTYPE_MAGIC_POINT},
		{"攻击力", worlddb.ATTRTYPE_ATTACK_VALUE},
		{"抵抗力", worlddb.ATTRTYPE_DEFENSE_VALUE},
		{"击中率", worlddb.ATTRTYPE_HIT_CHANCE},
		{"闪避率", worlddb.ATTRTYPE_DODGE_CHANCE},
		{"暴击率", worlddb.ATTRTYPE_CRITIHIT_CHANCE},
		{"暴击抵抗率", worlddb.ATTRTYPE_CRITIHIT_CHANCE_RESIST},
		{"暴击强度", worlddb.ATTRTYPE_CRITIHIT_INTENSITY},
		{"暴击抵抗强度", worlddb.ATTRTYPE_CRITIHIT_INTENSITY_RESIST},
	}
}

func getViewPlayerCareer() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.PlayerCareer_None},
		{"人族", worlddb.PlayerCareer_Terran},
		{"神族", worlddb.PlayerCareer_Protoss},
		{"魔族", worlddb.PlayerCareer_Demons},
	}
}

func getViewPlayerGender() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.PlayerGender_None},
		{"男性", worlddb.PlayerGender_Male},
		{"女性", worlddb.PlayerGender_Female},
	}
}

func getViewCurrencyType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.CurrencyType_None},
		{"金币", worlddb.CurrencyType_Gold},
		{"钻石", worlddb.CurrencyType_Diamond},
	}
}

func getViewChequeType() []webSelectOption {
	return []webSelectOption{
		{"无", worlddb.ChequeType_None},
		{"金币", worlddb.ChequeType_Gold},
		{"钻石", worlddb.ChequeType_Diamond},
		{"经验", worlddb.ChequeType_Exp},
	}
}

func getViewItemFlowType() []webSelectOption {
	return []webSelectOption{
		{"无", 0},
		{"采集", 3},
		{"商店", 4},
	}
}
