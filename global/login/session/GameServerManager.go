package session

import (
	"test.com/common"
)

var InstGameServerMgr *GameServerManager

type GameServerInstance struct {
	*common.GameServerInfo
	IsOnline bool
}

type GameServerManager struct {
	gsInstMap map[uint32]*GameServerInstance
}

func NewGameServerManagerInstance() {
	InstGameServerMgr = &GameServerManager{}
}
