package session

import (
	"encoding/gob"
	"log"

	"test.com/common"
	"test.com/fusion"
)

func (obj *CenterServerSession) handleRegisterResp(pck *fusion.NetPacket) int {
	obj.isReady = true
	log.Printf("register to center server `%s` successfully.\n", obj)
	return fusion.SessionHandleSuccess
}

func (obj *CenterServerSession) handlePushGameServerList(pck *fusion.NetPacket) int {
	var gsNum uint32
	pck.Read(&gsNum)
	gsInstMap := make(map[uint32]*GameServerInstance, gsNum)
	for i := 0; i < int(gsNum); i++ {
		gsInst := GameServerInstance{GameServerInfo: &common.GameServerInfo{}}
		gob.NewDecoder(pck.GetReader()).Decode(gsInst.GameServerInfo)
		pck.Read(&gsInst.IsOnline)
		gsInstMap[gsInst.ID] = &gsInst
	}
	InstGameServerMgr.gsInstMap = gsInstMap
	return fusion.SessionHandleSuccess
}

func (obj *CenterServerSession) handlePushGameServerStatus(pck *fusion.NetPacket) int {
	var isOnline bool
	pck.Read(&isOnline)
	for !pck.IsReadableEmpty() {
		var gsID uint32
		pck.Read(&gsID)
		gsInst, isOK := InstGameServerMgr.gsInstMap[gsID]
		if isOK {
			gsInst.IsOnline = isOnline
		}
	}
	return fusion.SessionHandleSuccess
}
