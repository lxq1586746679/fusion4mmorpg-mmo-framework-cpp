package main

import (
	"math/rand"
	"time"

	"test.com/common"
	"test.com/fusion"
	"test.com/login/session"
)

var cfg userConfig
var inst mainService

type userConfig struct {
	WebClientListen   string
	LoginServerListen string
	CenterServerHost  string
	GlobalDB          fusion.MysqlConfig
	RedisDB           fusion.RedisConfig
}

type mainService struct {
	fusion.ServiceBase
}

func main() {
	newSingletonInstances()
	fusion.LoadUserConfig("config.json", &cfg)
	common.GlobalDB = fusion.OpenMysql(&cfg.GlobalDB, 32, 1)
	common.RedisDB = fusion.OpenRedis(&cfg.RedisDB)
	fusion.InitGoPool(32)
	fusion.StartService(&inst.ServiceBase, "main")
	go fusion.SafeHandler(runCenterServerConnector)()
	go fusion.SafeHandler(runWebClientListener)()
	fusion.MainRunAndWaitQuit()
}

func newSingletonInstances() {
	rand.Seed(time.Now().UnixNano())
	session.NewCenterServerSessionInstance()
	session.NewGameServerManagerInstance()
}
