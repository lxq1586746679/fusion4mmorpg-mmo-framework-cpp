package worlddb

type QuestQuestion struct {
	Id uint32 `json:"Id,omitempty" rule:"required"`
}

func (*QuestQuestion) GetTableName() string {
	return "quest_question"
}
func (*QuestQuestion) GetTableKeyName() string {
	return "Id"
}
func (obj *QuestQuestion) GetTableKeyValue() uint {
	return uint(obj.Id)
}
