package worlddb

//#include "struct_base.h"

type PayShop struct {
	Id            uint32       `json:"Id,omitempty" rule:"required"`
	StrName       string       `json:"strName,omitempty" rule:"required"`
	PlatformId    uint32       `json:"platformId,omitempty" rule:"required"`
	StrChannels   string       `json:"strChannels,omitempty" rule:"required"`
	ItemType      uint32       `json:"itemType,omitempty" rule:"required"`
	BuyPrice      uint32       `json:"buyPrice,omitempty" rule:"required"`
	BuyGoldValue  uint32       `json:"buyGoldValue,omitempty" rule:"required"`
	BuyCheques    []ChequeInfo `json:"buyCheques,omitempty" rule:"required"`
	BuyItems      []FItemInfo  `json:"buyItems,omitempty" rule:"required"`
	BuyShowItemId uint32       `json:"buyShowItemId,omitempty" rule:"required"`
	StrRemark     string       `json:"strRemark,omitempty" rule:"required"`
}

func (*PayShop) GetTableName() string {
	return "pay_shop"
}
func (*PayShop) GetTableKeyName() string {
	return "Id"
}
func (obj *PayShop) GetTableKeyValue() uint {
	return uint(obj.Id)
}
