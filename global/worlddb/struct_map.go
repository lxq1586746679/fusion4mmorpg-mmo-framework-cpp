package worlddb

// MapInfo_Type
const (
	MapInfo_Type_VoidSpace   = 0
	MapInfo_Type_WorldMap    = 1
	MapInfo_Type_Story       = 2
	MapInfo_Type_PlayBoss    = 3
	MapInfo_Type_GuildLeague = 4
	MapInfo_Type_Count       = 5
)

type MapInfo_Flags struct {
	IsParallelMap       bool `json:"isParallelMap,omitempty"`
	IsRecoveryHPDisable bool `json:"isRecoveryHPDisable,omitempty"`
}

type MapInfo struct {
	Id               uint32        `json:"Id,omitempty" rule:"required"`
	Type             uint32        `json:"type,omitempty" rule:"required"`
	Flags            MapInfo_Flags `json:"flags,omitempty" rule:"required"`
	StrName          string        `json:"strName,omitempty" rule:"required"`
	StrSceneFile     string        `json:"strSceneFile,omitempty" rule:"required"`
	Viewing_distance float32       `json:"viewing_distance,omitempty" rule:"required"`
	Load_value       uint32        `json:"load_value,omitempty" rule:"required"`
	Pop_map_id       uint32        `json:"pop_map_id,omitempty" rule:"required"`
	Pop_pos_x        float32       `json:"pop_pos_x,omitempty" rule:"required"`
	Pop_pos_y        float32       `json:"pop_pos_y,omitempty" rule:"required"`
	Pop_pos_z        float32       `json:"pop_pos_z,omitempty" rule:"required"`
	Pop_o            float32       `json:"pop_o,omitempty" rule:"required"`
}

func (*MapInfo) GetTableName() string {
	return "map_info"
}
func (*MapInfo) GetTableKeyName() string {
	return "Id"
}
func (obj *MapInfo) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type MapZone struct {
	Id        uint32  `json:"Id,omitempty" rule:"required"`
	ParentId  uint32  `json:"parentId,omitempty" rule:"required"`
	Priority  uint32  `json:"priority,omitempty" rule:"required"`
	Map_id    uint32  `json:"map_id,omitempty" rule:"required"`
	Map_type  uint32  `json:"map_type,omitempty" rule:"required"`
	X1        float32 `json:"x1,omitempty" rule:"required"`
	Y1        float32 `json:"y1,omitempty" rule:"required"`
	Z1        float32 `json:"z1,omitempty" rule:"required"`
	X2        float32 `json:"x2,omitempty" rule:"required"`
	Y2        float32 `json:"y2,omitempty" rule:"required"`
	Z2        float32 `json:"z2,omitempty" rule:"required"`
	Pvp_flags uint32  `json:"pvp_flags,omitempty" rule:"required"`
}

func (*MapZone) GetTableName() string {
	return "map_zone"
}
func (*MapZone) GetTableKeyName() string {
	return "Id"
}
func (obj *MapZone) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type MapGraveyard struct {
	Id       uint32  `json:"Id,omitempty" rule:"required"`
	Name     string  `json:"name,omitempty" rule:"required"`
	Map_id   uint32  `json:"map_id,omitempty" rule:"required"`
	Map_type uint32  `json:"map_type,omitempty" rule:"required"`
	X        float32 `json:"x,omitempty" rule:"required"`
	Y        float32 `json:"y,omitempty" rule:"required"`
	Z        float32 `json:"z,omitempty" rule:"required"`
	O        float32 `json:"o,omitempty" rule:"required"`
	Trait    uint32  `json:"trait,omitempty" rule:"required"`
}

func (*MapGraveyard) GetTableName() string {
	return "map_graveyard"
}
func (*MapGraveyard) GetTableKeyName() string {
	return "Id"
}
func (obj *MapGraveyard) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type TeleportPoint struct {
	Id       uint32  `json:"Id,omitempty" rule:"required"`
	Name     string  `json:"name,omitempty" rule:"required"`
	Map_id   uint32  `json:"map_id,omitempty" rule:"required"`
	Map_type uint32  `json:"map_type,omitempty" rule:"required"`
	X        float32 `json:"x,omitempty" rule:"required"`
	Y        float32 `json:"y,omitempty" rule:"required"`
	Z        float32 `json:"z,omitempty" rule:"required"`
	O        float32 `json:"o,omitempty" rule:"required"`
	Trait    uint32  `json:"trait,omitempty" rule:"required"`
}

func (*TeleportPoint) GetTableName() string {
	return "teleport_point"
}
func (*TeleportPoint) GetTableKeyName() string {
	return "Id"
}
func (obj *TeleportPoint) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type WayPoint struct {
	Id          uint32  `json:"Id,omitempty" rule:"required"`
	First_wp_id uint32  `json:"first_wp_id,omitempty" rule:"required"`
	Prev_wp_id  uint32  `json:"prev_wp_id,omitempty" rule:"required"`
	Next_wp_id  uint32  `json:"next_wp_id,omitempty" rule:"required"`
	Map_id      uint32  `json:"map_id,omitempty" rule:"required"`
	X           float32 `json:"x,omitempty" rule:"required"`
	Y           float32 `json:"y,omitempty" rule:"required"`
	Z           float32 `json:"z,omitempty" rule:"required"`
	Keep_idle   int32   `json:"keep_idle,omitempty" rule:"required"`
	Emote_state uint32  `json:"emote_state,omitempty" rule:"required"`
}

func (*WayPoint) GetTableName() string {
	return "way_point"
}
func (*WayPoint) GetTableKeyName() string {
	return "Id"
}
func (obj *WayPoint) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type LandmarkPoint struct {
	Id       uint32  `json:"Id,omitempty" rule:"required"`
	Name     string  `json:"name,omitempty" rule:"required"`
	Map_id   uint32  `json:"map_id,omitempty" rule:"required"`
	Map_type uint32  `json:"map_type,omitempty" rule:"required"`
	X        float32 `json:"x,omitempty" rule:"required"`
	Y        float32 `json:"y,omitempty" rule:"required"`
	Z        float32 `json:"z,omitempty" rule:"required"`
}

func (*LandmarkPoint) GetTableName() string {
	return "landmark_point"
}
func (*LandmarkPoint) GetTableKeyName() string {
	return "Id"
}
func (obj *LandmarkPoint) GetTableKeyValue() uint {
	return uint(obj.Id)
}
