package worlddb

type ShopPrototype struct {
	Id             uint32 `json:"Id,omitempty" rule:"required"`
	ShopType       uint32 `json:"shopType,omitempty" rule:"required"`
	ItemTypeID     uint32 `json:"itemTypeID,omitempty" rule:"required"`
	ItemCount      uint32 `json:"itemCount,omitempty" rule:"required"`
	ItemFlags      uint32 `json:"itemFlags,omitempty" rule:"required"`
	CurrencyType   uint32 `json:"currencyType,omitempty" rule:"required"`
	SellPrice      uint32 `json:"sellPrice,omitempty" rule:"required"`
	DiscountPrice  uint32 `json:"discountPrice,omitempty" rule:"required"`
	BundleNumMax   uint32 `json:"bundleNumMax,omitempty" rule:"required"`
	GenderLimit    uint32 `json:"genderLimit,omitempty" rule:"required"`
	CareerLimit    uint32 `json:"careerLimit,omitempty" rule:"required"`
	DailyBuyLimit  uint32 `json:"dailyBuyLimit,omitempty" rule:"required"`
	WeeklyBuyLimit uint32 `json:"weeklyBuyLimit,omitempty" rule:"required"`
}

func (*ShopPrototype) GetTableName() string {
	return "shop_prototype"
}
func (*ShopPrototype) GetTableKeyName() string {
	return "Id"
}
func (obj *ShopPrototype) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type SpecialShopPrototype struct {
	ShopPrototype

	ItemUniqueKey        uint64 `json:"itemUniqueKey,omitempty"`
	ItemBeginTime        int64  `json:"itemBeginTime,omitempty"`
	ItemEndTime          int64  `json:"itemEndTime,omitempty"`
	ServerBuyLimit       uint32 `json:"serverBuyLimit,omitempty"`
	CharBuyLimit         uint32 `json:"charBuyLimit,omitempty"`
	DailyServerBuyLimit  uint32 `json:"dailyServerBuyLimit,omitempty"`
	WeeklyServerBuyLimit uint32 `json:"weeklyServerBuyLimit,omitempty"`
}
