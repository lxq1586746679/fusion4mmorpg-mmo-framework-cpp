package worlddb

// ScriptType
const (
	ScriptType_None         = 0
	ScriptType_AIGraphml    = 1
	ScriptType_CharSpawn    = 2
	ScriptType_CharDead     = 3
	ScriptType_CharPlay     = 4
	ScriptType_SObjSpawn    = 11
	ScriptType_SObjPlay     = 12
	ScriptType_QuestReq     = 21
	ScriptType_QuestInit    = 22
	ScriptType_QuestReward  = 23
	ScriptType_QuestEvent   = 24
	ScriptType_CanCastSpell = 31
	ScriptType_UseItem      = 41
)

type Scriptable struct {
	ScriptId   uint32 `json:"scriptId,omitempty" rule:"required"`
	ScriptType uint32 `json:"scriptType,omitempty" rule:"required"`
	ScriptFile string `json:"scriptFile,omitempty" rule:"required"`
}

func (*Scriptable) GetTableName() string {
	return "scriptable"
}
func (*Scriptable) GetTableKeyName() string {
	return "scriptId"
}
func (obj *Scriptable) GetTableKeyValue() uint {
	return uint(obj.ScriptId)
}
