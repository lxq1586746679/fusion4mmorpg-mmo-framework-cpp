package main

import (
	"log"
	"net"

	"test.com/center/session"
	"test.com/fusion"
	"test.com/protocol"
)

func runGameServerListener() {
	var addr = cfg.GameServerListen
	log.Printf("start game server listener `%s` goroutine successfully.\n", addr)

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("listen game `%s` failed, %s.\n", addr, err)
	}

	log.Printf("listen game server `%s` successfully.\n", addr)
	fusion.AllClosers.Store(ln, true)

	for !fusion.IsStopService {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("accept game `%s` failed, %s.\n", addr, err)
			continue
		}
		log.Printf("accept game server `%s` ...\n", conn.RemoteAddr())
		go fusion.SafeConnHandler(runGameServerHandler)(conn)
	}
}

func runGameServerHandler(conn net.Conn) {
	var obj session.GameSession
	obj.InitAndStartSendCoroutine(&inst.ServiceBase, conn)
	defer func() {
		session.InstGameServerMgr.UnregisterGameServer(&obj)
		obj.ShutdownAndStopSendCoroutine(false)
	}()
	obj.RunRecvCoroutine(protocol.CCT_RPC_INVOKE_RESP, func(pck *fusion.NetPacket) int {
		if !checkGameServerReadyStatus(&obj, pck.Opcode) {
			log.Printf("handle game `%s` packet ready status is invalid.\n", &obj)
			return fusion.SessionHandleUnhandle
		}
		var handler = session.GameSessionHandlers[pck.Opcode]
		if handler != nil {
			return handler(&obj, pck)
		}
		var rpcHandler = session.GameSessionRPCHandlers[pck.Opcode]
		if rpcHandler != nil {
			return rpcHandler(&obj, pck, fusion.ReadRPCReqMetaInfo(pck))
		}
		return fusion.SessionHandleUnhandle
	})
}

func checkGameServerReadyStatus(obj *session.GameSession, opcode int) bool {
	if opcode > protocol.FLAG_CENTER_PROTOCOL_FOR_GAME_BEGIN {
		return obj.ReadyStatus == session.GS_READY_STATUS_FOR_GAME
	}
	if opcode > protocol.FLAG_CENTER_PROTOCOL_FOR_GATE_BEGIN {
		return obj.ReadyStatus == session.GS_READY_STATUS_FOR_GATE
	}
	if opcode > protocol.FLAG_CENTER_PROTOCOL_NEED_REGISTER_BEGIN {
		return obj.ReadyStatus != session.GS_READY_STATUS_NONE
	}
	return true
}
