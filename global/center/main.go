package main

import (
	"log"
	"math/rand"
	"time"

	"test.com/center/session"
	"test.com/common"
	"test.com/fusion"
)

var cfg userConfig
var inst mainService

type userConfig struct {
	WebGmServerListen string
	LoginServerListen string
	GameServerListen  string
	GlobalDB          fusion.MysqlConfig
	RedisDB           fusion.RedisConfig
}

type mainService struct {
	fusion.ServiceBase
}

func main() {
	newSingletonInstances()
	fusion.LoadUserConfig("config.json", &cfg)
	common.GlobalDB = fusion.OpenMysql(&cfg.GlobalDB, 32, 1)
	common.RedisDB = fusion.OpenRedis(&cfg.RedisDB)
	fusion.InitGoPool(32)
	fusion.StartService(&inst.ServiceBase, "main")
	loadDataFromDB()
	go fusion.SafeHandler(runLoginServerListener)()
	go fusion.SafeHandler(runGameServerListener)()
	go fusion.SafeHandler(runWebGmServerListener)()
	fusion.MainRunAndWaitQuit()
}

func loadDataFromDB() {
	session.InstGameServerMgr.LoadDataFromDB()
	log.Printf("load all game server config successfully.\n")
}

func newSingletonInstances() {
	rand.Seed(time.Now().UnixNano())
	session.NewLoginServerManagerInstance()
	session.NewGameServerManagerInstance()
}
