package session

import (
	"test.com/common"
	"test.com/fusion"
	"test.com/protocol"
)

func finishLoginRPCResp(errCode int32, errMsg string) *fusion.NetPacket {
	return finishResp(protocol.SLC_RPC_INVOKE_RESP, errCode, errMsg)
}

func (obj *LoginServerSession) handleRegister(pck *fusion.NetPacket) int {
	if err := InstLoginServerMgr.RegisterLoginServer(obj); err != nil {
		obj.SendPacket(finishResp(protocol.SLC_REGISTER_RESP,
			common.GsErrorCodeRegisterFailed, err.Error()))
		return fusion.SessionHandleKill
	}
	resp := finishResp(protocol.SLC_REGISTER_RESP, common.GsErrorCodeNone, "")
	obj.SendPacket(resp)
	obj.SendPacket(InstGameServerMgr.BuildAllGameServerInfosPacket())
	return fusion.SessionHandleSuccess
}
