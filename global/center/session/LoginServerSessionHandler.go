package session

import (
	"test.com/fusion"
	"test.com/protocol"
)

var LoginServerSessionHandlers = [protocol.LOGIN2CENTER_MESSAGE_COUNT]func(*LoginServerSession, *fusion.NetPacket) int{
	protocol.CLC_REGISTER: (*LoginServerSession).handleRegister,
}

var LoginServerSessionRPCHandlers = [protocol.LOGIN2CENTER_MESSAGE_COUNT]func(*LoginServerSession, *fusion.NetPacket, *fusion.RPCReqMetaInfo) int{}
