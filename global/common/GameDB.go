package common

import (
	"database/sql"

	"github.com/go-redis/redis"
)

var GlobalDB *sql.DB
var RedisDB *redis.Client
