#include "preHeader.h"
#include "DBPSessionHandler.h"
#include "protocol/DBPProtocol.h"

DBPSessionHandler::DBPSessionHandler()
{
	handlers_[DBPProtocol::SDBP_REGISTER_RESP] = &DBPSessionHandler::HandleRegisterResp;
};

DBPSessionHandler::~DBPSessionHandler()
{
}
