#include "preHeader.h"
#include "ShopDef.h"

const ShopItemStatus defaultShopItemStatus;

bool IsDefaultShopItemStatus(const ShopItemStatus& siStatus)
{
	return siStatus.totalCount == 0 &&
		siStatus.dailyCount == 0 &&
		siStatus.weeklyCount == 0;
}

bool IsShopPrototypeBuyLimit(const ShopPrototype* pProto)
{
	return pProto->dailyBuyLimit != 0 ||
		pProto->weeklyBuyLimit != 0;
}

bool IsSpecialShopPrototypeServerBuyLimit(const SpecialShopPrototype* pProto)
{
	return pProto->serverBuyLimit != 0 ||
		pProto->dailyServerBuyLimit != 0 ||
		pProto->weeklyServerBuyLimit != 0;
}

bool IsSpecialShopPrototypeCharBuyLimit(const SpecialShopPrototype* pProto)
{
	return pProto->charBuyLimit != 0 ||
		pProto->dailyBuyLimit != 0 ||
		pProto->weeklyBuyLimit != 0;
}

uint32 GetBuyShopItemMaxCount4BuyLimit(
	const ShopPrototype* pProto, const ShopItemStatus& siStatus)
{
	uint32 buyShopItemCounts[] = {
		pProto->dailyBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->dailyBuyLimit, siStatus.dailyCount),
		pProto->weeklyBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->weeklyBuyLimit, siStatus.weeklyCount)
	};
	return *std::min_element(
		std::begin(buyShopItemCounts), std::end(buyShopItemCounts));
}

uint32 GetBuySpecialShopItemMaxCount4ServerBuyLimit(
	const SpecialShopPrototype* pProto, const ShopItemStatus& siStatus)
{
	uint32 buyShopItemCounts[] = {
		pProto->serverBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->serverBuyLimit, siStatus.totalCount),
		pProto->dailyServerBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->dailyServerBuyLimit, siStatus.dailyCount),
		pProto->weeklyServerBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->weeklyServerBuyLimit, siStatus.weeklyCount)
	};
	return *std::min_element(
		std::begin(buyShopItemCounts), std::end(buyShopItemCounts));
}

uint32 GetBuySpecialShopItemMaxCount4CharBuyLimit(
	const SpecialShopPrototype* pProto, const ShopItemStatus& siStatus)
{
	uint32 buyShopItemCounts[] = {
		pProto->charBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->charBuyLimit, siStatus.totalCount),
		pProto->dailyBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->dailyBuyLimit, siStatus.dailyCount),
		pProto->weeklyBuyLimit == 0 ? BUY_SHOP_ITEM_COUNT :
			SubLeastZero(pProto->weeklyBuyLimit, siStatus.weeklyCount)
	};
	return *std::min_element(
		std::begin(buyShopItemCounts), std::end(buyShopItemCounts));
}

void ApplyBuyShopItem4BuyLimit(
	const ShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num)
{
	if (pProto->dailyBuyLimit != 0) {
		siStatus.dailyCount += num;
	}
	if (pProto->weeklyBuyLimit != 0) {
		siStatus.weeklyCount += num;
	}
}

void ApplyBuySpecialShopItem4ServerBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num)
{
	if (pProto->serverBuyLimit != 0) {
		siStatus.totalCount += num;
	}
	if (pProto->dailyServerBuyLimit != 0) {
		siStatus.dailyCount += num;
	}
	if (pProto->weeklyServerBuyLimit != 0) {
		siStatus.weeklyCount += num;
	}
}

void ApplyBuySpecialShopItem4CharBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num)
{
	if (pProto->charBuyLimit != 0) {
		siStatus.totalCount += num;
	}
	if (pProto->dailyBuyLimit != 0) {
		siStatus.dailyCount += num;
	}
	if (pProto->weeklyBuyLimit != 0) {
		siStatus.weeklyCount += num;
	}
}

void RevertBuySpecialShopItem4ServerBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num)
{
	if (pProto->serverBuyLimit != 0) {
		SubLeastZeroX(siStatus.totalCount, num);
	}
	if (pProto->dailyServerBuyLimit != 0) {
		SubLeastZeroX(siStatus.dailyCount, num);
	}
	if (pProto->weeklyServerBuyLimit != 0) {
		SubLeastZeroX(siStatus.weeklyCount, num);
	}
}
