#pragma once

#define GLOBAL_COOLDOWN_KEY (0)

#define SPELL_SELECT_TYPE_ARGS \
	std::unordered_set<ObjGUID>& targetList, \
	const SpellLevelEffectInfo& sleiInfo

#define SPELL_SELECT_MODE_ARGS \
	const std::function<bool(Unit*)>& test, \
	const vector3f& tgtEffPos, \
	std::unordered_set<ObjGUID>& targetList, \
	const SpellLevelEffectInfo& sleiInfo

enum class CanCastSpellFlag
{
	CheckDistance = 1 << 0,
};

struct SpellLevelEffectPrototype
{
	const SpellLevelEffectInfo* sleiInfo;
};

struct SpellLevelPrototype
{
	const SpellLevelInfo* sliInfo;
	std::vector<SpellLevelEffectPrototype> sleiList;
};

struct SpellPrototype
{
	const SpellInfo* siInfo;
	std::vector<SpellLevelPrototype> sliList;
};

struct SpellPropInfo {
	enum Flag {
		GuildSpell = 1 << 0,
	};
	const SpellPrototype* pSpellProto;
	uint32 spellLevel;
	uint32 flags;
};

enum CooldownType
{
	COOLDOWN_TYPE_SPELL,
	COOLDOWN_TYPE_CATEGORY,
	COOLDOWN_TYPE_GLOBAL,
	COOLDOWN_TYPE_COUNT
};

struct SpellCooldownInfo
{
	const SpellPrototype* pSpellProto;
	uint64 RecoveryTime;
	uint64 ExpireTime;
};
