#include "preHeader.h"
#include "QuestDef.h"

inst_quest_prop::inst_quest_prop()
: questGuid(0)
, questTypeID(0)
, questFlags(0)
, questExpireTime(0)
{
}

template <typename Packer>
void save_inst_quest_prop(const inst_quest_prop& questProp, Packer& packer)
{
	packer << questProp.questGuid << questProp.questTypeID
		<< questProp.questFlags << questProp.questExpireTime;
	packer << (u8)questProp.questConditions.size();
	for (auto& condition : questProp.questConditions) {
		packer << condition.progress << condition.userData;
	}
}

template <typename Unpacker>
void load_inst_quest_prop(inst_quest_prop& questProp, Unpacker& unpacker)
{
	unpacker >> questProp.questGuid >> questProp.questTypeID
		>> questProp.questFlags >> questProp.questExpireTime;
	questProp.questConditions.resize(UnpackValue<u8>(unpacker));
	for (auto& condition : questProp.questConditions) {
		unpacker >> condition.progress >> condition.userData;
	}
}

void inst_quest_prop::Save(INetStream& pck) const
{
	save_inst_quest_prop(*this, pck);
}

void inst_quest_prop::Load(INetStream& pck)
{
	load_inst_quest_prop(*this, pck);
}

void inst_quest_prop::Save(TextPacker& packer) const
{
	save_inst_quest_prop(*this, packer);
}

void inst_quest_prop::Load(TextUnpacker& unpacker)
{
	load_inst_quest_prop(*this, unpacker);
}

bool IsCareQuestStatus(QUEST_STATUS questStatus)
{
	static const QUEST_STATUS allCareQuestStatus[] = {
		QMGR_QUEST_AVAILABLELOW_LEVEL, QMGR_QUEST_AVAILABLE,
	};
	return IS_ARRAY_CONTAIN_VALUE(allCareQuestStatus, questStatus);
}
