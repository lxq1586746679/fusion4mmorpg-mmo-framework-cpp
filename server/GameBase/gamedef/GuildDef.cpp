#include "preHeader.h"
#include "GuildDef.h"

const int GuildTitleAuthorities[] = {
	(int)GUILD_TITLE_AUTHORITY::MASTER,
	(int)GUILD_TITLE_AUTHORITY::SUBMASTER,
	(int)GUILD_TITLE_AUTHORITY::OFFICER,
	(int)GUILD_TITLE_AUTHORITY::MEMBER,
};
STATIC_ASSERT(ARRAY_SIZE(GuildTitleAuthorities)==(int)GUILD_TITLE::COUNT);
