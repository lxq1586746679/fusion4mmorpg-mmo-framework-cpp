#pragma once

union ObjGUID
{
	uint64 objGUID;
	struct {
		uint32 UID;
		uint16 TID;
		uint16 SID;
	};
	bool operator==(const ObjGUID& other) const {
		return objGUID == other.objGUID;
	}
	bool operator!=(const ObjGUID& other) const {
		return objGUID != other.objGUID;
	}
	bool operator<(const ObjGUID& other) const {
		return objGUID < other.objGUID;
	}
};

union InstGUID
{
	uint64 instGUID;
	struct {
		uint32 UID;
		uint16 MAPID;
		uint16 TID;
	};
	bool operator==(const InstGUID& other) const {
		return instGUID == other.instGUID;
	}
	bool operator!=(const InstGUID& other) const {
		return instGUID != other.instGUID;
	}
	bool operator<(const InstGUID& other) const {
		return instGUID < other.instGUID;
	}
};

namespace std {
	template <> struct hash<ObjGUID> {
		std::size_t operator()(const ObjGUID& arg) const {
			return arg.objGUID;
		}
	};
	template <> struct hash<InstGUID> {
		std::size_t operator()(const InstGUID& arg) const {
			return arg.instGUID;
		}
	};
}
