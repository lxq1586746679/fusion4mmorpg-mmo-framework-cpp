#pragma once

#define UNIT_AROUND_ENEMY_SLOT_MIN (5)
#define UNIT_AROUND_ENEMY_SWITCH_DIST (2.f)
#define UNIT_AROUND_ENEMY_GRAB_DIST (3.f)
#define UNIT_AROUND_ENEMY_START_DIST (5.f)

struct EnemyInfo
{
	ObjGUID enemy;
	uint32 hatred;
};

struct EnemySlot
{
	ObjGUID enemy;
	vector3f slotDir;
};

enum MoveMode
{
	MoveByWalk,
	MoveByRun,
};

enum MoveChangeType
{
	MoveChangeKeepDist,
	MoveChangeFacePos,
	MoveChangeMoveMode,
};

enum ArenaTeamSide
{
	teamInvalid = -1,
	teamBegin = 0,
	teamRed = teamBegin,
	teamBlue,
	teamCount
};
