#include "preHeader.h"
#include "MapServerPacketHandler.h"
#include "protocol/InternalProtocol.h"

MapServerPacketHandler::MapServerPacketHandler()
{
	handlers_[MapServer2CrossServer::SMC_START_INSTANCE] = &MapServerPacketHandler::HandleStartInstance;
	handlers_[MapServer2CrossServer::SMC_STOP_INSTANCE] = &MapServerPacketHandler::HandleStopInstance;
	rpc_handlers_[MapServer2CrossServer::SMC_QUERY_PLAYER_LEVEL_MAX] = &MapServerPacketHandler::HandleQueryPlayerLevelMax;
};

MapServerPacketHandler::~MapServerPacketHandler()
{
}
