#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GameServerSession;

class GameServerSessionHandler :
	public MessageHandler<GameServerSessionHandler, GameServerSession, MapServer2GameServer::MAP2GAME_MESSAGE_COUNT>,
	public Singleton<GameServerSessionHandler>
{
public:
	GameServerSessionHandler();
	virtual ~GameServerSessionHandler();
private:
	int HandleRegisterResp(GameServerSession *pSession, INetPacket &pck);
	int HandlePushServerId(GameServerSession *pSession, INetPacket &pck);
	int HandleStartWorldMap(GameServerSession *pSession, INetPacket &pck);
	int HandleStartInstance(GameServerSession *pSession, INetPacket &pck);
	int HandleStopInstance(GameServerSession *pSession, INetPacket &pck);
	int HandleCharacterTeleportBeginEnterInstance(GameServerSession *pSession, INetPacket &pck);
	int HandleSyncPlayerLevelMax(GameServerSession *pSession, INetPacket &pck);
	int HandleSyncOperatingCares(GameServerSession *pSession, INetPacket &pck);
};

#define sGameServerSessionHandler (*GameServerSessionHandler::instance())
