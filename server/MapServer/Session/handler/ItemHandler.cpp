#include "preHeader.h"
#include "Session/PlayerPacketHandler.h"
#include "Map/MapInstance.h"

int PlayerPacketHandler::HandleUseItem(Player *pPlayer, INetPacket &pck)
{
	uint32 slot, num;
	std::string_view udata;
	pck >> slot >> num;
	if (!pck.IsReadableEmpty()) {
		pck >> udata;
	}
	auto errCode = pPlayer->GetItemStorage()->UseItem(slot, num, udata);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleDestroyItem(Player *pPlayer, INetPacket &pck)
{
	uint32 slot, num;
	pck >> slot >> num;
	auto errCode = pPlayer->GetItemStorage()->DestroyItem(slot, num);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleArrangeItems(Player *pPlayer, INetPacket &pck)
{
	int32 type;
	pck >> type;
	auto errCode = pPlayer->GetItemStorage()->ArrangeItems((ItemSlotType)type);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleSwapItem(Player *pPlayer, INetPacket &pck)
{
	int32 type, tgtType;
	uint32 slot, tgtSlot;
	pck >> type >> slot >> tgtType >> tgtSlot;
	auto errCode = pPlayer->GetItemStorage()->SwapItem(
		(ItemSlotType)type, slot, (ItemSlotType)tgtType, tgtSlot);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleSplitItem(Player *pPlayer, INetPacket &pck)
{
	int32 type;
	uint32 slot, num;
	pck >> type >> slot >> num;
	auto errCode = pPlayer->GetItemStorage()->SplitItem((ItemSlotType)type, slot, num);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleEquipItem(Player *pPlayer, INetPacket &pck)
{
	uint32 slot;
	pck >> slot;
	auto errCode = pPlayer->GetItemStorage()->EquipItem(slot);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleUnequipItem(Player *pPlayer, INetPacket &pck)
{
	uint32 slot;
	pck >> slot;
	auto errCode = pPlayer->GetItemStorage()->UnequipItem(slot);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}
