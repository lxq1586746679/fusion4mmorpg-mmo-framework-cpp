#include "preHeader.h"
#include "Session/CrossServerPacketHandler.h"
#include "Cross/CrossServer.h"
#include "Cross/PlayBossMgr.h"

int CrossServerPacketHandler::HandlePlaybossChangeTeam(CrossServerService* pService, INetPacket& pck)
{
	ObjGUID playerGuid;
	ClientAddr4Cross clientAddr;
	uint32 teamId;
	std::string_view teamName;
	pck >> playerGuid >> clientAddr >> teamId >> teamName;
	sPlayBossMgr.HandlePlayerChangeTeam(playerGuid, clientAddr, teamId, teamName);
	return SessionHandleSuccess;
}

int CrossServerPacketHandler::HandlePlaybossBroadcast(CrossServerService* pService, INetPacket& pck)
{
	sPlayBossMgr.HandleBroadcast(pck);
	return SessionHandleSuccess;
}

int CrossServerPacketHandler::HandlePlaybossStrike(CrossServerService* pService, INetPacket& pck)
{
	bool isPlayer;
	InstGUID instGuid;
	ClientAddr4Cross clientAddr;
	ObjGUID playerGuid;
	uint32 teamId, spawnId;
	std::string_view playerName, teamName;
	uint64 loseHP;
	pck >> instGuid >> spawnId >> loseHP >> isPlayer;
	if (isPlayer) {
		pck >> clientAddr >> playerGuid >> playerName
			>> teamId >> teamName;
	}
	sPlayBossMgr.HandlePlayerStrike(
		instGuid, clientAddr, isPlayer ? playerGuid : ObjGUID_NULL,
		playerName, teamId, teamName, spawnId, loseHP);
	return SessionHandleSuccess;
}

int CrossServerPacketHandler::HandlePlaybossSync(CrossServerService* pService, INetPacket& pck)
{
	InstGUID instGuid;
	int32 syncType;
	pck >> instGuid >> syncType;
	sPlayBossMgr.HandleInstanceSync(instGuid, (PlayBossSyncType)syncType, pck);
	return SessionHandleSuccess;
}

int CrossServerPacketHandler::HandlePlaybossJoin(CrossServerService* pService, INetPacket& pck)
{
	ObjGUID playerGuid;
	ClientAddr4Cross clientAddr;
	pck >> playerGuid >> clientAddr;
	auto errCode = sPlayBossMgr.HandlePlayerJoin(playerGuid);
	if (errCode != CommonSuccess) {
		sCrossServer.SendError2Client(clientAddr, errCode);
	}
	return SessionHandleSuccess;
}
