#pragma once

#include "Singleton.h"
#include "Session/DBPSession.h"

class DBPServerMgr : public Singleton<DBPServerMgr>
{
public:
	DBPServerMgr();
	virtual ~DBPServerMgr();

	void CheckConnections();

	void AddDBPServer(const std::string& host, const std::string& port);

	DBPSession* GetDBPSession(size_t index) const;
	const std::vector<DBPSession*>& GetDBPServerList() const {
		return m_DBPServerList;
	}

private:
	std::vector<DBPSession*> m_DBPServerList;
};

#define sDBPServerMgr (*DBPServerMgr::instance())
#define DBPSession(i) (*sDBPServerMgr.GetDBPSession(i))
