#pragma once

#include "Singleton.h"
#include "network/Listener.h"

class GateServerListener : public Listener, public Singleton<GateServerListener>
{
public:
	THREAD_RUNTIME(GateServerListener)

	GateServerListener();
	virtual ~GateServerListener();

private:
	virtual std::string GetBindAddress();
	virtual std::string GetBindPort();

	virtual Session *NewSessionObject();
};

#define sGateServerListener (*GateServerListener::instance())
