#pragma once

#include "IMapHook.h"

class EmptyHook : public IMapHook
{
public:
	EmptyHook(MapInstance* pMapInstance);
	virtual ~EmptyHook();
};
