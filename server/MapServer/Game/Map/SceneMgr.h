#pragma once

#include "Singleton.h"
#include "Scene.h"

class SceneMgr : public Singleton<SceneMgr>
{
public:
	SceneMgr();
	virtual ~SceneMgr();

	Scene* CreateAndGetScene(const std::string& sceneDir);

private:
	Scene* GetScene(const std::string& sceneDir);

	std::shared_mutex m_sceneMutex;
	std::unordered_map<std::string, Scene*> m_scenes;

	std::mutex m_loadingMutex;
	std::condition_variable_any m_loadingCV;
	std::unordered_set<std::string> m_sceneLoading;
};

#define sSceneMgr (*SceneMgr::instance())
