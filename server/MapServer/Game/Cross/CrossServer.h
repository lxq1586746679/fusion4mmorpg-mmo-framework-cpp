#pragma once

#include "Singleton.h"
#include "FrameWorker.h"
#include "Service/ServerService.h"
#include "struct/teleport.h"

class CrossServer : public FrameWorker, public AsyncTaskOwner,
	public Singleton<CrossServer>
{
public:
	THREAD_RUNTIME(CrossServer)

	CrossServer();
	virtual ~CrossServer();

	void SendError2Client(const ClientAddr4Cross& clientAddr, GErrorCode error);

	void TransPacket2Instance(InstGUID instGuid, const INetPacket& pck);
	void TransPacket2Player(ObjGUID playerGuid, const INetPacket& pck);
	void TransPacket2Client(const ClientAddr4Cross& clientAddr, const INetPacket& pck);
	void TransPacket2GameServer(uint32 gsIdx, const INetPacket& pck);

	void StartInstance(InstGUID fakeInstGuid,
		ObjGUID instOwner = ObjGUID_NULL, uint32 opCodeResp = OPCODE_NONE,
		uint32 flags = 0, const std::string_view& args = emptyStringView);
	void StopInstance(InstGUID instGuid);

	void TeleportPlayer(ObjGUID playerGuid, InstGUID instGuid, const vector3f1f& tgtPos,
		TeleportType tpType = TeleportType::SwitchMap, uint32 tpFlags = 0,
		const std::string_view& initArgs = emptyStringView);

	IServerService* GetService() const { return m_pService; }

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual bool Initialize();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	IServerService* const m_pService;
};

#define sCrossServer (*CrossServer::instance())
