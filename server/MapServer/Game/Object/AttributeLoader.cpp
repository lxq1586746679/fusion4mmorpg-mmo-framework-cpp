#include "preHeader.h"
#include "Creature.h"
#include "Player.h"

void Player::LoadBase(double attrs[])
{
	auto pAttribute = GetDBEntry<PlayerAttribute>(GetAttributeId());
	if (pAttribute != NULL) {
		std::copy_n(pAttribute->attrs.begin(), (int)ATTRTYPE::COUNT, attrs);
	}
}

void Player::LoadBody(AttrPartProxy& proxy)
{
}

void Player::LoadEquip(AttrPartProxy& proxy)
{
	m_pItemStorage->ForeachItem(ItemSlotEquipType, [&proxy](uint32, Item* pItem) {
		pItem->ApplyAttributes(proxy);
	});
}

void Player::LoadExtraAttrs()
{
	auto pAttribute = GetDBEntry<PlayerAttribute>(GetAttributeId());
	if (pAttribute != NULL) {
		m_attribute.SetAttrEx(ATTREXTYPE::DAMAGE_FACTOR, pAttribute->damageFactor);
	}
	auto pPrototype = GetDBEntry<PlayerBase>(GetLevel());
	if (pPrototype != NULL) {
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_RATE, pPrototype->recoveryHPRate);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_VALUE, pPrototype->recoveryHPValue);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_RATE, pPrototype->recoveryMPRate);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_VALUE, pPrototype->recoveryMPValue);
	}
}

uint32 Player::GetAttributeId() const
{
	return ((u32)GetCareer() << 24) + GetLevel();
}

void Creature::LoadBase(double attrs[])
{
	auto pAttribute = GetDBEntry<CreatureAttribute>(GetAttributeId());
	if (pAttribute != NULL) {
		std::copy_n(pAttribute->attrs.begin(), (int)ATTRTYPE::COUNT, attrs);
	}
}

void Creature::LoadBody(AttrPartProxy& proxy)
{
}

void Creature::LoadEquip(AttrPartProxy& proxy)
{
}

void Creature::LoadExtraAttrs()
{
	auto pAttribute = GetDBEntry<CreatureAttribute>(GetAttributeId());
	if (pAttribute != NULL) {
		m_attribute.SetAttrEx(ATTREXTYPE::DAMAGE_FACTOR, pAttribute->damageFactor);
	}
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_RATE, m_pProto->recoveryHPRate);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_VALUE, m_pProto->recoveryHPValue);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_RATE, m_pProto->recoveryMPRate);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_VALUE, m_pProto->recoveryMPValue);
}

uint64 Creature::GetAttributeId() const
{
	auto attrType = m_pProto->attrType != 0 ? m_pProto->attrType : m_pProto->charElite;
	return ((u64)GetRound() << 48) + ((u64)GetLevel() << 32) + attrType;
}
