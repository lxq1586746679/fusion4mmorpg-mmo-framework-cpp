#pragma once

#include "Player.h"

class AutoPlayer : public Player
{
public:
	AutoPlayer();
	virtual ~AutoPlayer();
};
