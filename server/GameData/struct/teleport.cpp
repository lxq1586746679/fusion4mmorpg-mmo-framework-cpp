#include "jsontable/table_helper.h"
#include "teleport.h"

CharTeleportInfo::CharTeleportInfo()
: playerGuid(0)
, ownerGuid(0)
, instGuid(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, type(0)
, flags(0)
{
}

CharLoadInfo::CharLoadInfo()
: playerGuid(0)
, type(0)
, flags(0)
{
}

PlayerTeleportInfo::PlayerTeleportInfo()
: playerGuid(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, guildId(0)
, guildTitle(0)
, teamId(0)
, type(0)
, flags(0)
, clientSN(0)
, gateSN(0)
{
}
