#include "jsontable/table_helper.h"
#include "actvt_string.h"

template<> const char *GetTableName<actvt_string_text_list>()
{
	return "actvt_string_text_list";
}

template<> const char *GetTableKeyName<actvt_string_text_list>()
{
	return "stringID";
}

template<> uint64 GetTableKeyValue(const actvt_string_text_list &entity)
{
	return (uint64)entity.stringID;
}

template<> void SetTableKeyValue(actvt_string_text_list &entity, uint64 key)
{
	entity.stringID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<actvt_string_text_list>(size_t index)
{
	switch (index)
	{
		case 0: return "stringID";
		case 1: return "stringEN";
		case 2: return "stringCN";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<actvt_string_text_list>(const char *name)
{
	if (strcmp(name, "stringID") == 0) return 0;
	if (strcmp(name, "stringEN") == 0) return 1;
	if (strcmp(name, "stringCN") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<actvt_string_text_list>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const actvt_string_text_list &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.stringID);
		case 1: return StringHelper::ToString(entity.stringEN);
		case 2: return StringHelper::ToString(entity.stringCN);
	}
	return "";
}

template<> void SetTableFieldValue(actvt_string_text_list &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.stringID, value);
		case 1: return StringHelper::FromString(entity.stringEN, value);
		case 2: return StringHelper::FromString(entity.stringCN, value);
	}
}

template<> void LoadFromStream(actvt_string_text_list &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.stringID, stream);
	StreamHelper::FromStream(entity.stringEN, stream);
	StreamHelper::FromStream(entity.stringCN, stream);
}

template<> void SaveToStream(const actvt_string_text_list &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.stringID, stream);
	StreamHelper::ToStream(entity.stringEN, stream);
	StreamHelper::ToStream(entity.stringCN, stream);
}

template<> void LoadFromText(actvt_string_text_list &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const actvt_string_text_list &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(actvt_string_text_list &entity, const rapidjson::Value &value)
{
	FromJson(entity.stringID, value, "stringID");
	FromJson(entity.stringEN, value, "stringEN");
	FromJson(entity.stringCN, value, "stringCN");
}

template<> void JsonHelper::BlockToJson(const actvt_string_text_list &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.stringID, value, "stringID");
	ToJson(entity.stringEN, value, "stringEN");
	ToJson(entity.stringCN, value, "stringCN");
}
