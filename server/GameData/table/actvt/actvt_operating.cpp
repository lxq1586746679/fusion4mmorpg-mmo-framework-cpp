#include "jsontable/table_helper.h"
#include "actvt_operating.h"

OperatingActivity::OperatingActivity()
: nId(0)
, nUIType(0)
, nType(0)
, nTimes(0)
, nPreType(0)
, nPreTimes(0)
, nShowReqVipLevel(0)
, nShowReqLevel(0)
, nPlayReqVipLevel(0)
, nPlayReqLevel(0)
, nActvtReferTime(0)
, nActvtStopTime(0)
, nShowStartTime(0)
, nShowEndTime(0)
, nPlayStartTime(0)
, nPlayEndTime(0)
, nDailyStartTime(0)
, nDailyEndTime(0)
{
}

OperatingActivityUIType::OperatingActivityUIType()
: nId(0)
, nUIType(0)
{
}

OperatingChargeSelfSelect::RewardData::RewardData()
{
}

OperatingChargeSelfSelect::OperatingChargeSelfSelect()
: gradVal(0)
, maxTimes(0)
{
}

OperatingAccumulativeCharge::RewardData::RewardData()
: gradVal(0)
{
}

OperatingAccumulativeCharge::OperatingAccumulativeCharge()
{
}
