#pragma once

struct social_friend
{
	social_friend();

	uint32 characterId;
	uint32 friendId;
	int64 createTime;
};

struct social_ignore
{
	social_ignore();

	uint32 characterId;
	uint32 ignoreId;
	int64 createTime;
};
