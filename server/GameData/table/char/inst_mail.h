#pragma once

enum class MAIL_RQST_TYPE
{
	COUNT_ALL,
	LIST_SOMES,
	GET_ATTACHMENT,
	VIEW_DETAIL,
	WRITE_ORDER,
	DELETE_ORDER,

	SEND_SINGLE,
	SEND_MULTI_SAME,
	SEND_MULTI_DIFFERENT,
};

enum class MAIL_TYPE
{
	NORMAL,
	SYSTEM,
};

enum class MailFlag
{
	SubjectArgs = 1 << 0,
	BodyArgs = 1 << 1,
};

struct InstMailAttachment
{
	InstMailAttachment();

	uint32 mailID;
	std::vector<std::string> mailCheques;
	std::vector<std::string> mailItems;
};

struct inst_mail
{
	inst_mail();

	uint32 mailID;
	uint32 mailType;
	uint32 mailFlags;
	uint32 mailSender;
	std::string mailSenderName;
	uint32 mailReceiver;
	int64 mailDeliverTime;
	int64 mailExpireTime;
	std::string mailSubject;
	std::string mailBody;
	std::vector<std::string> mailCheques;
	std::vector<std::string> mailItems;
	bool isGetAttachment;
	bool isViewDetail;
};
