#include "jsontable/table_helper.h"
#include "inst_player_char.h"

IpcFlags::IpcFlags()
: isInit(false)
{
}

IpcAppearance::IpcAppearance()
: charTypeId(0)
, hairType(0)
, hairColor(0)
, face(0)
, skin(0)
{
}

IpcPropertyValue::IpcPropertyValue()
: bagCapacity(0)
, bankCapacity(0)
{
}

IpcRankValue::IpcRankValue()
: lastLevelTime(0)
, lastFightValueTime(0)
{
}

IpcGsReadValue::IpcGsReadValue()
: lastFightValue(0)
{
}

IpcGsWriteValue::IpcGsWriteValue()
: guildScore(0)
{
}

InstPlayerPreviewInfo::InstPlayerPreviewInfo()
: ipcInstID(0)
, ipcCareer(0)
, ipcGender(0)
, ipcLevel(0)
, ipcVipLevel(0)
, ipcMoneyGold(0)
, ipcMoneyDiamond(0)
{
}

InstPlayerOutlineInfo::InstPlayerOutlineInfo()
: ipcInstID(0)
, ipcAcctID(0)
, ipcCareer(0)
, ipcGender(0)
, ipcMapType(0)
, ipcMapID(0)
, ipcPosX(.0f)
, ipcPosY(.0f)
, ipcPosZ(.0f)
, ipcPosO(.0f)
, ipcLevel(0)
, ipcVipLevel(0)
, ipcServerID(0)
, ipcLastOnlineTime(0)
{
}

inst_player_char::inst_player_char()
: ipcInstID(0)
, ipcAcctID(0)
, ipcCareer(0)
, ipcGender(0)
, ipcMapType(0)
, ipcMapID(0)
, ipcPosX(.0f)
, ipcPosY(.0f)
, ipcPosZ(.0f)
, ipcPosO(.0f)
, ipcVipLevel(0)
, ipcLevel(0)
, ipcExp(0)
, ipcCurHP(0)
, ipcCurMP(0)
, ipcMoneyGold(0)
, ipcMoneyDiamond(0)
, ipcServerID(0)
, ipcCreateTime(0)
, ipcLastLoginTime(0)
, ipcLastOnlineTime(0)
, ipcDeleteTime(0)
{
}
