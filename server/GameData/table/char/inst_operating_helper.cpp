#include "jsontable/table_helper.h"
#include "inst_operating.h"

template<> const char *GetTableName<OperatingActivityData>()
{
	return "inst_operating_activities";
}

template<> const char *GetTableKeyName<OperatingActivityData>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const OperatingActivityData &entity)
{
	return 0;
}

template<> void SetTableKeyValue(OperatingActivityData &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<OperatingActivityData>(size_t index)
{
	switch (index)
	{
		case 0: return "playerId";
		case 1: return "actType";
		case 2: return "actTimes";
		case 3: return "saveData";
		case 4: return "userData";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<OperatingActivityData>(const char *name)
{
	if (strcmp(name, "playerId") == 0) return 0;
	if (strcmp(name, "actType") == 0) return 1;
	if (strcmp(name, "actTimes") == 0) return 2;
	if (strcmp(name, "saveData") == 0) return 3;
	if (strcmp(name, "userData") == 0) return 4;
	return -1;
}

template<> size_t GetTableFieldNumber<OperatingActivityData>()
{
	return 5;
}

template<> std::string GetTableFieldValue(const OperatingActivityData &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.playerId);
		case 1: return StringHelper::ToString(entity.actType);
		case 2: return StringHelper::ToString(entity.actTimes);
		case 3: return JsonHelper::SequenceToJsonText(entity.saveData);
		case 4: return StringHelper::ToString(entity.userData);
	}
	return "";
}

template<> void SetTableFieldValue(OperatingActivityData &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.playerId, value);
		case 1: return StringHelper::FromString(entity.actType, value);
		case 2: return StringHelper::FromString(entity.actTimes, value);
		case 3: return JsonHelper::SequenceFromJsonText(entity.saveData, value);
		case 4: return StringHelper::FromString(entity.userData, value);
	}
}

template<> void LoadFromStream(OperatingActivityData &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerId, stream);
	StreamHelper::FromStream(entity.actType, stream);
	StreamHelper::FromStream(entity.actTimes, stream);
	StreamHelper::SequenceFromStream(entity.saveData, stream);
	StreamHelper::FromStream(entity.userData, stream);
}

template<> void SaveToStream(const OperatingActivityData &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerId, stream);
	StreamHelper::ToStream(entity.actType, stream);
	StreamHelper::ToStream(entity.actTimes, stream);
	StreamHelper::SequenceToStream(entity.saveData, stream);
	StreamHelper::ToStream(entity.userData, stream);
}

template<> void LoadFromText(OperatingActivityData &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingActivityData &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingActivityData &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerId, value, "playerId");
	FromJson(entity.actType, value, "actType");
	FromJson(entity.actTimes, value, "actTimes");
	SequenceFromJson(entity.saveData, value, "saveData");
	FromJson(entity.userData, value, "userData");
}

template<> void JsonHelper::BlockToJson(const OperatingActivityData &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerId, value, "playerId");
	ToJson(entity.actType, value, "actType");
	ToJson(entity.actTimes, value, "actTimes");
	SequenceToJson(entity.saveData, value, "saveData");
	ToJson(entity.userData, value, "userData");
}
