#pragma once

#include "inst_configure.h"
#include "inst_guild.h"
#include "inst_mail.h"
#include "inst_operating.h"
#include "inst_player_char.h"
#include "inst_rank.h"
#include "inst_social.h"
