enum SpellStageType
{
	Chant,
	Channel,
	Cleanup,
}

enum SpellResumeType
{
	RemoveWhenOffline,
	ContinueWhenOnline,
	ContinueByTime,
}

enum SpellEffectStyle
{
	None,
	Positive,
	Negative,
	All,
}

enum SpellInterruptBy
{
	None,
	Move,
	Injured,
}

enum SpellEffectType
{
	None = 0,
	Attack,
	AuraAttack,
	ChangeProp,
	ChangeSpeed,
	Invincible,
	Invisible,
	Fetter,
	Stun,

	AuraObject = 300,

	Teleport = 500,
	Mine,
}

enum SpellTargetType
{
	Any,
	Friend,
	Enemy,
	TeamMember,
	Count
}

enum SpellSelectType
{
	None,
	Self,
	Friend,
	Enemy,
	Friends,
	Enemies,
	SelfOrFriend,
	FocusFriends,
	FocusEnemies,
	TeamMember,
	TeamMembers,
	SelfOrTeamMember,
	FocusTeamMembers,
	Reference,
	Count
}

enum SpellSelectMode
{
	Circle,
	Sector,
	Rect,
	Count
}

enum SpellPassiveMode
{
	Status,
	Event,
	Count
}

enum SpellPassiveBy
{
	StatusNone = 0,
	StatusHPValue,
	StatusHPRate,
	StatusMax,

	EventHit = 0,
	EventHitBy,
	EventMax,
}

table SpellInfo
{
	struct Flags {
		bool isExclusive;
		bool isPassive;
		bool isAppearance;
		bool isSync2Client;
		bool isSync2AllClient;
		bool isTargetDeadable;
		bool isCheckCastDist;
		bool isIgnoreOutOfControl;
	}
	required uint32 spellID;
	required string spellIcon;
	required Flags spellFlags;
	required uint8 spellBuffType;
	required uint8 spellCooldownType;
	required uint8 spellTargetType;
	required uint8 spellPassiveMode;
	required uint8 spellPassiveBy;
	required float[] spellPassiveArgs;
	required float spellPassiveChance;
	required uint32 spellLimitCareer;
	required uint32 spellLimitMapType;
	required uint32 spellLimitMapID;
	required uint32 spellLimitScriptID;
	required string spellLimitScriptArgs;
	required uint32 spellInterruptBys;
	required uint32[] spellStageTime;
	(key=spellID, tblname=spell_info)
}

table SpellLevelInfo
{
	required uint32 spellLevelID;
	required uint32 spellID;
	required uint32 spellLimitLevel;
	required float spellCastDistMin;
	required float spellCastDistMax;
	required uint32 spellEvalScore;
	required uint32 spellCostHP;
	required float spellCostHPRate;
	required uint32 spellCostMP;
	required float spellCostMPRate;
	required uint32 spellCDTime;
	(key=spellLevelID, tblname=spell_level_info)
}

table SpellLevelEffectInfo
{
	struct Flags {
		bool isResumable;
		bool isDeadResumable;
		bool isSync2Client;
		bool isSync2AllClient;
		bool isTargetDeadable;
		bool isClientSelectTarget;
	}
	required uint32 spellLevelEffectID;
	required uint32 spellID;
	required uint32 spellLevelID;
	required Flags spellEffectFlags;
	required uint8 spellStageTrigger;
	required uint32 spellDelayTrigger;
	required uint8 spellSelectType;
	required uint8 spellSelectMode;
	required float[] spellSelectArgs;
	required uint32 spellSelectNumber;
	required uint32 spellDelayEffective;
	required float spellEffectiveChance;
	required uint32 spellEffectStyle;
	required uint32 spellEffectType;
	required string spellEffectArgs;
	(key=spellLevelEffectID, tblname=spell_level_effect_info)
}

enum AuraSelectType
{
	Friend,
	Enemy,
	Player,
	Creature,
	TeamMember,
	Count
}