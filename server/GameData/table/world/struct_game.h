#pragma once

struct GameTime
{
	GameTime();

	enum class Type {
		None,
		PlayBoss,
		GuildLeague,
	};
	uint32 Id;
	std::string strTime;
	std::string strDesc;
};
