#pragma once

struct LootSet
{
	LootSet();

	uint32 lsID;
	std::string lsName;
};

struct LootSetGroup
{
	LootSetGroup();

	uint32 lsgID;
	uint32 lsID;
	std::string lsgName;
	float lsgOdds;
	uint32 lsgMinItemTimes;
	uint32 lsgMaxItemTimes;
	uint32 lsgMinChequeTimes;
	uint32 lsgMaxChequeTimes;
};

struct LootSetGroupItem
{
	LootSetGroupItem();

	struct Flags {
		Flags();
		bool bindToPicker;
	};
	uint32 lsgiID;
	uint32 lsID;
	uint32 lsgID;
	Flags lsgiFlags;
	uint32 lsgiWeight;
	uint32 lsgiItemTypeID;
	uint32 lsgiMinCount;
	uint32 lsgiMaxCount;
	uint32 lsgiMaxTimes;
	uint32 lsgiLimitQuest;
	uint32 lsgiLimitCareer;
	uint32 lsgiLimitGender;
};

struct LootSetGroupCheque
{
	LootSetGroupCheque();

	uint32 lsgcID;
	uint32 lsID;
	uint32 lsgID;
	uint32 lsgcWeight;
	uint8 lsgcChequeType;
	uint64 lsgcMinValue;
	uint64 lsgcMaxValue;
	uint32 lsgcMaxTimes;
};
