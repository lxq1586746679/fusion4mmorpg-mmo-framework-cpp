#include "jsontable/table_helper.h"
#include "struct_loot.h"

LootSet::LootSet()
: lsID(0)
{
}

LootSetGroup::LootSetGroup()
: lsgID(0)
, lsID(0)
, lsgOdds(.0f)
, lsgMinItemTimes(0)
, lsgMaxItemTimes(0)
, lsgMinChequeTimes(0)
, lsgMaxChequeTimes(0)
{
}

LootSetGroupItem::Flags::Flags()
: bindToPicker(false)
{
}

LootSetGroupItem::LootSetGroupItem()
: lsgiID(0)
, lsID(0)
, lsgID(0)
, lsgiWeight(0)
, lsgiItemTypeID(0)
, lsgiMinCount(0)
, lsgiMaxCount(0)
, lsgiMaxTimes(0)
, lsgiLimitQuest(0)
, lsgiLimitCareer(0)
, lsgiLimitGender(0)
{
}

LootSetGroupCheque::LootSetGroupCheque()
: lsgcID(0)
, lsID(0)
, lsgID(0)
, lsgcWeight(0)
, lsgcChequeType(0)
, lsgcMinValue(0)
, lsgcMaxValue(0)
, lsgcMaxTimes(0)
{
}
