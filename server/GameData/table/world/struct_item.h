#pragma once

enum class ItemClass
{
	None,
	Equip,  // 装备
	Quest,  // 任务
	Material,  // 材料
	Gemstone,  // 宝石
	Consumable,  // 消耗品
	Score,  // 积分
	Scrap,  // 废品
	Other,  // 其它
};

enum class ItemSubClass
{
	Equip_Weapon = 1,  // 武器
	Equip_Belt,  // 腰带
	Equip_Gloves,  // 手套
	Equip_Shoes,  // 鞋子
};

enum class ItemQuality
{
	White,
	Red,
	Count,
};

struct ItemPrototype
{
	ItemPrototype();

	struct Flags {
		Flags();
		bool canUse;
		bool canSell;
		bool canDestroy;
		bool isDestroyAfterUse;
	};

	uint32 itemTypeID;
	Flags itemFlags;
	uint32 itemClass;
	uint32 itemSubClass;
	uint32 itemQuality;
	uint32 itemLevel;
	uint32 itemStack;

	uint32 itemSellPrice;

	uint32 itemLootId;
	uint32 itemSpellId;
	uint32 itemSpellLevel;
	uint32 itemScriptId;
	std::string itemScriptArgs;
	std::vector<uint32> itemParams;
};


struct ItemEquipPrototype
{
	ItemEquipPrototype();

	uint32 itemTypeID;

	struct Attr {
		Attr();
		uint32 type;
		double value;
	};
	std::vector<Attr> itemEquipAttrs;

	struct Spell {
		Spell();
		uint32 id;
		uint32 level;
	};
	std::vector<Spell> itemEquipSpells;
};
