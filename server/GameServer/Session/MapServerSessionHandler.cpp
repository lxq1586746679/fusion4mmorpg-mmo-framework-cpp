#include "preHeader.h"
#include "MapServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

MapServerSessionHandler::MapServerSessionHandler()
{
	handlers_[MapServer2GameServer::MS_REGISTER] = &MapServerSessionHandler::HandleRegister;
	handlers_[MapServer2GameServer::MS_TO_SOCIAL_SERVER_PACKET] = &MapServerSessionHandler::HandleToSocialServerPacket;
	handlers_[MapServer2GameServer::MS_KICK] = &MapServerSessionHandler::HandleKick;
	handlers_[MapServer2GameServer::MS_CHARACTER_SAVED] = &MapServerSessionHandler::HandleCharacterSaved;
	handlers_[MapServer2GameServer::MS_UPDATE_CHARACTER_INFO] = &MapServerSessionHandler::HandleUpdateCharacterInfo;
	handlers_[MapServer2GameServer::MS_SWITCH_MAP] = &MapServerSessionHandler::HandleSwitchMap;
	handlers_[MapServer2GameServer::MS_CHARACTER_TELEPORT_BEGIN_ENTER_INSTANCE_RESULT] = &MapServerSessionHandler::HandleCharacterTeleportBeginEnterInstanceResult;
	handlers_[MapServer2GameServer::MS_CHARACTER_ENTER_MAP_RESP] = &MapServerSessionHandler::HandleCharacterEnterMapResp;
	handlers_[MapServer2GameServer::MS_CHARACTER_LEAVE_MAP_RESP] = &MapServerSessionHandler::HandleCharacterLeaveMapResp;
	handlers_[MapServer2GameServer::MS_UPDATE_ACTIVITY_OBJECT_POSITION] = &MapServerSessionHandler::HandleUpdateActivityObjectPosition;
	rpc_handlers_[MapServer2GameServer::MS_QUERY_ACTIVITY_OBJECT_POSITION] = &MapServerSessionHandler::HandleQueryActivityObjectPosition;
	handlers_[MapServer2GameServer::MS_MAP_TEAM_CREATE] = &MapServerSessionHandler::HandleMapTeamCreate;
	handlers_[MapServer2GameServer::MS_MAIL] = &MapServerSessionHandler::HandleMail;
	handlers_[MapServer2GameServer::MS_FINISH_GUILD_LEAGUE] = &MapServerSessionHandler::HandleFinishGuildLeague;
};

MapServerSessionHandler::~MapServerSessionHandler()
{
}
