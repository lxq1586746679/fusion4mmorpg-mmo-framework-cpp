#include "preHeader.h"
#include "Session/ClientPacketHandler.h"
#include "Session/MapServerMgr.h"
#include "Game/Account.h"

int ClientPacketHandler::HandlePlaybossJoin(Account* pAccount, INetPacket& pck)
{
	auto pChar = pAccount->GetCharacter();
	NetPacket trans(CMC_PLAYBOSS_JOIN);
	const InstGUID instGuid = MakeInstGuid(
		(u16)MapInfo::Type::PlayBoss, sGameConfig.PlayBossMapId);
	sMapServerMgr.SendPacket2CrossServer4Player(pChar, instGuid, trans);
	return SessionHandleSuccess;
}
