#include "preHeader.h"
#include "Session/ClientPacketHandler.h"
#include "Team/TeamMgr.h"
#include "Game/Account.h"

int ClientPacketHandler::HandleTeamCreate(Account *pAccount, INetPacket &pck)
{
	auto errCode = sTeamMgr.HandleTeamCreate(pAccount->GetCharacter());
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamInvite(Account *pAccount, INetPacket &pck)
{
	ObjGUID targetGuid;
	pck >> targetGuid;
	auto errCode = sTeamMgr.HandleTeamInvite(pAccount->GetCharacter(), targetGuid);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamInviteResp(Account *pAccount, INetPacket &pck)
{
	uint32 teamId;
	bool isAgree;
	pck >> teamId >> isAgree;
	auto errCode = sTeamMgr.HandleTeamInviteResp(pAccount->GetCharacter(), teamId, isAgree);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamApply(Account *pAccount, INetPacket &pck)
{
	uint32 teamId;
	pck >> teamId;
	auto errCode = sTeamMgr.HandleTeamApply(pAccount->GetCharacter(), teamId);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamApplyResp(Account *pAccount, INetPacket &pck)
{
	ObjGUID applicantGuid;
	bool isAgree;
	pck >> applicantGuid >> isAgree;
	auto errCode = sTeamMgr.HandleTeamApplyResp(pAccount->GetCharacter(), applicantGuid, isAgree);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamLeave(Account *pAccount, INetPacket &pck)
{
	auto errCode = sTeamMgr.HandleTeamLeave(pAccount->GetCharacter());
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamKick(Account *pAccount, INetPacket &pck)
{
	ObjGUID targetGuid;
	pck >> targetGuid;
	auto errCode = sTeamMgr.HandleTeamKick(pAccount->GetCharacter(), targetGuid);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int ClientPacketHandler::HandleTeamTransfer(Account *pAccount, INetPacket &pck)
{
	ObjGUID targetGuid;
	pck >> targetGuid;
	auto errCode = sTeamMgr.HandleTeamTransfer(pAccount->GetCharacter(), targetGuid);
	if (errCode != CommonSuccess) {
		pAccount->SendError(errCode);
	}
	return SessionHandleSuccess;
}
