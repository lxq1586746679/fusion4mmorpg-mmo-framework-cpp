#include "preHeader.h"
#include "MapServerListener.h"
#include "MapServerSession.h"
#include "ServerMaster.h"

MapServerListener::MapServerListener()
: m_sn(0)
{
}

MapServerListener::~MapServerListener()
{
}

std::string MapServerListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("MAP_SERVER", "HOST", "0.0.0.0");
}

std::string MapServerListener::GetBindPort()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("MAP_SERVER", "PORT", "9997");
}

Session *MapServerListener::NewSessionObject()
{
	return new MapServerSession(++m_sn);
}
