#pragma once

#include "Singleton.h"

class DataMgr : public Singleton<DataMgr>
{
public:
	DataMgr();
	virtual ~DataMgr();

	void LoadData();

public:
	time_t getStartServerTime() const { return m_startServerTime; }
private:
	void InitStartServerTime();
	time_t m_startServerTime;

public:
	void setOperatingCares(const std::string& operatingCares);
	void saveLastOperatingEventTime(int64 lastOperatingEventTime);
	bool isOperatingCare(int i) const { return m_operatingCares.test(i); }
	const auto& getOperatingCares() const { return m_operatingCares; }
	time_t getLastOperatingEventTime() const { return m_lastOperatingEventTime; }
private:
	void LoadLastOperatingEventTime();
	time_t m_lastOperatingEventTime;
	std::bitset<(int)OperatingCareType::Count> m_operatingCares;
};

#define sDataMgr (*DataMgr::instance())
