#include "preHeader.h"
#include "GameSession.h"
#include "GameSessionHandler.h"
#include "DBPServer.h"

MysqlConnection emptyMysqlConnection;

GameSession::GameSession()
: RPCSession("GameSession", OPCODE_NONE)
, m_isReady(false)
{
}

GameSession::~GameSession()
{
}

int GameSession::HandlePacket(INetPacket *pck)
{
	if (m_isReady || pck->GetOpcode() < FLAG_DBP_PROTOCOL_NEED_REGISTER_BEGIN) {
		return sGameSessionHandler.HandlePacket(this, *pck);
	} else {
		return SessionHandleUnhandle;
	}
}

void GameSession::OnShutdownSession()
{
	enable_linked_from_this::disable_linked_from_this();
	RPCSession::OnShutdownSession();
}

void GameSession::Feedback(const std::weak_ptr<GameSession>& gsPtr,
	const INetStream& data, uint64 sn, int32 err, bool eof)
{
	ConstNetPacket pck(SDBP_RPC_INVOKE_RESP,
		data.GetReadableBuffer(), data.GetReadableSize());
	if (!gsPtr.expired()) {
		gsPtr.lock()->RPCReply(pck, sn, err, eof);
	}
}

void GameSession::FeedbackError(const std::weak_ptr<GameSession>& gsPtr,
	uint64 sn, int32 err)
{
	Feedback(gsPtr, ConstNetBuffer(), sn, err);
}

int GameSessionHandler::HandleRegister(GameSession *pSession, INetPacket &pck)
{
	NetPacket resp(SDBP_REGISTER_RESP);
	pSession->PushSendPacket(resp);

	pSession->m_isReady = true;

	return SessionHandleSuccess;
}
