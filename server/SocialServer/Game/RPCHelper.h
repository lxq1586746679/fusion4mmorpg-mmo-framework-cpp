#pragma once

class RPCHelper
{
public:
	static std::unordered_map<uint32, std::string_view> ReadReplyDataDict(INetStream& pck);
	static const std::string_view& TryGetReplyData(
		const std::unordered_map<uint32, std::string_view>& datas, uint32 key);

	static GErrorCode RPCPullPlayerInfo(Coroutine::YieldContext& ctx,
		uint32 playerId, uint32 flags, std::string_view& rst,
		AsyncTaskOwner *owner = nullptr, time_t timeout = DEF_S2S_RPC_TIMEOUT);
	static GErrorCode RPCPullPlayerInfos(Coroutine::YieldContext& ctx,
		const uint32 playerIds[], size_t playerNum, uint32 flags,
		std::unordered_map<uint32, std::string_view>& rst,
		AsyncTaskOwner *owner = nullptr, time_t timeout = DEF_S2S_RPC_TIMEOUT);
	static void PackRPCPlayerInfos(INetPacket& pck,
		const uint32 playerIds[], size_t playerNum,
		const std::unordered_map<uint32, std::string_view>& rst);
};
