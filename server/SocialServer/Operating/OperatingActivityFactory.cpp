#include "preHeader.h"
#include "OperatingActivityFactory.h"
#include "ChargeSelfSelect.h"
#include "AccumulativeCharge.h"

OperatingActivityBase* OperatingActivityFactory::Create(const OperatingActivity* pProto)
{
	switch ((OperatingActivityType)pProto->nType) {
	case OperatingActivityType::ChargeSelfSelect:
		return new ChargeSelfSelect(pProto);
	case OperatingActivityType::AccumulativeCharge:
		return new AccumulativeCharge(pProto);
	default:
		assert(false && "can't reach here.");
		return NULL;
	}
}
