#include "preHeader.h"
#include "SocialServerMaster.h"

int main(int argc, char *argv[])
{
	SocialServerMaster::newInstance();
	sSocialServerMaster.InitSingleton();

	if (sSocialServerMaster.Initialize(argc, argv) == 0) {
		sSocialServerMaster.Run(argc, argv);
	}

	sSocialServerMaster.FinishSingleton();
	SocialServerMaster::deleteInstance();

	printf("Social Server shutdown gracefully!\n");

	return 0;
}
