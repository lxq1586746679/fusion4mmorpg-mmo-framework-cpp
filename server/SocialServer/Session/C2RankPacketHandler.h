#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/OpCode.h"

class RankMgr;

class C2RankPacketHandler :
	public MessageHandler<C2RankPacketHandler, RankMgr, GAME_OPCODE::CSMSG_COUNT, uint32>,
	public Singleton<C2RankPacketHandler>
{
public:
	C2RankPacketHandler();
	virtual ~C2RankPacketHandler();
private:
	int HandleRankGetPlayerList(RankMgr *mgr, INetPacket &pck, uint32 uid);
	int HandleRankGetGuildList(RankMgr *mgr, INetPacket &pck, uint32 uid);
};

#define sC2RankPacketHandler (*C2RankPacketHandler::instance())
