#include "preHeader.h"
#include "C2OperatingPacketHandler.h"
#include "protocol/OpCode.h"

C2OperatingPacketHandler::C2OperatingPacketHandler()
{
	handlers_[GAME_OPCODE::CMSG_OPERATING_GET_TYPE] = &C2OperatingPacketHandler::HandleOperatingGetType;
};

C2OperatingPacketHandler::~C2OperatingPacketHandler()
{
}
