#include "preHeader.h"
#include "GateServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GateServerSessionHandler::GateServerSessionHandler()
{
	handlers_[GateServer2SocialServer::CGT_REGISTER] = &GateServerSessionHandler::HandleRegister;
	handlers_[GateServer2SocialServer::CGT_UPDATE_GS_GATE_SN] = &GateServerSessionHandler::HandleUpdateGsGateSn;
};

GateServerSessionHandler::~GateServerSessionHandler()
{
}
