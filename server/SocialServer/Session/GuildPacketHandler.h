#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GuildMgr;

class GuildPacketHandler :
	public MessageHandler<GuildPacketHandler, GuildMgr, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<GuildPacketHandler>
{
public:
	GuildPacketHandler();
	virtual ~GuildPacketHandler();
private:
	int HandleGetAllCharacterGuild(GuildMgr *mgr, INetPacket &pck);
};

#define sGuildPacketHandler (*GuildPacketHandler::instance())
