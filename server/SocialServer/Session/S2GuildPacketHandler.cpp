#include "preHeader.h"
#include "S2GuildPacketHandler.h"
#include "protocol/InternalProtocol.h"

S2GuildPacketHandler::S2GuildPacketHandler()
{
	handlers_[GameServer2SocialServer::CGX_UPDATE_GAME_SERVER] = &S2GuildPacketHandler::HandleUpdateGameServer;
	handlers_[GameServer2SocialServer::CGX_UPDATE_CHARACTER_INFO] = &S2GuildPacketHandler::HandleUpdateCharacterInfo;
	rpc_handlers_[GameServer2SocialServer::CGX_PULL_GUILD_INFOS] = &S2GuildPacketHandler::HandlePullGuildInfos;
	handlers_[GameServer2SocialServer::CGX_GET_ALL_GUILDS] = &S2GuildPacketHandler::HandleGetAllGuilds;
	handlers_[GameServer2SocialServer::CGX_GET_ALL_CHARACTER_GUILDS] = &S2GuildPacketHandler::HandleGetAllCharacterGuilds;
	rpc_handlers_[GameServer2SocialServer::CGX_TRY_GUILD_CREATE] = &S2GuildPacketHandler::HandleTryGuildCreate;
	rpc_handlers_[GameServer2SocialServer::CGX_GUILD_CREATE] = &S2GuildPacketHandler::HandleGuildCreate;
	handlers_[GameServer2SocialServer::CGX_GUILD_INVITE] = &S2GuildPacketHandler::HandleGuildInvite;
	handlers_[GameServer2SocialServer::CGX_GUILD_INVITE_RESP] = &S2GuildPacketHandler::HandleGuildInviteResp;
	handlers_[GameServer2SocialServer::CGX_GUILD_APPLY] = &S2GuildPacketHandler::HandleGuildApply;
	handlers_[GameServer2SocialServer::CGX_GUILD_APPLY_RESP] = &S2GuildPacketHandler::HandleGuildApplyResp;
	handlers_[GameServer2SocialServer::CGX_GUILD_LEAVE] = &S2GuildPacketHandler::HandleGuildLeave;
	handlers_[GameServer2SocialServer::CGX_GUILD_KICK] = &S2GuildPacketHandler::HandleGuildKick;
	handlers_[GameServer2SocialServer::CGX_GUILD_RISE] = &S2GuildPacketHandler::HandleGuildRise;
	rpc_handlers_[GameServer2SocialServer::CGX_GUILD_DISBAND] = &S2GuildPacketHandler::HandleGuildDisband;
	handlers_[GameServer2SocialServer::CGX_GUILD_GET_APPLY] = &S2GuildPacketHandler::HandleGuildGetApply;
};

S2GuildPacketHandler::~S2GuildPacketHandler()
{
}
