#pragma once

#include "network/Session.h"

class GateServerSessionHandler;

class GateServerSession : public Session
{
public:
	GateServerSession(uint32 sn);
	virtual ~GateServerSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnShutdownSession();

	bool IsReady() const { return m_gsGateSN != 0; }
	uint32 gsGateSN() const { return m_gsGateSN; }
	uint32 sn() const { return m_sn; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void TransServerPacket(INetPacket *pck) const;
	void TransClientPacket(INetPacket *pck) const;

	friend GateServerSessionHandler;
	const uint32 m_sn;
	uint32 m_gsGateSN;
};
