#include "preHeader.h"
#include "GameServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GameServerSessionHandler::GameServerSessionHandler()
{
	handlers_[GameServer2SocialServer::CGX_REGISTER] = &GameServerSessionHandler::HandleRegister;
	handlers_[GameServer2SocialServer::CGX_PUSH_SERVER_ID] = &GameServerSessionHandler::HandlePushServerId;
	handlers_[GameServer2SocialServer::CGX_RESTORE_CHARACTERS] = &GameServerSessionHandler::HandleRestoreCharacters;
	handlers_[GameServer2SocialServer::CGX_CHARACTER_ONLINE] = &GameServerSessionHandler::HandleCharacterOnline;
	handlers_[GameServer2SocialServer::CGX_CHARACTER_OFFLINE] = &GameServerSessionHandler::HandleCharacterOffline;
};

GameServerSessionHandler::~GameServerSessionHandler()
{
}
