#pragma once

#include "Singleton.h"
#include "network/Session.h"

class GameServerSessionHandler;

class GameServerSession : public Session, public Singleton<GameServerSession>
{
public:
	GameServerSession();
	virtual ~GameServerSession();

	void CheckConnection();

	void TransPacket(uint32 acct, const INetPacket& pck);

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void OnShutdownSession();
	virtual void DeleteObject();

	uint32 sn() const { return m_sn; }
	uint32 GetServerId() const { return m_serverId; }
	bool CanHandle(uint32 opcode) const { return m_handlerFlags[opcode]; }

	void CancelReadyStatus() { m_sn = 0; }
	bool IsReady() const { return m_sn != 0; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void TransClientPacket(INetPacket *pck) const;

	static void TryAddMapServer(INetPacket& pck);

	friend GameServerSessionHandler;
	uint32 m_serverId;
	uint32 m_sn;

	bool m_handlerFlags[GAME_OPCODE::CSMSG_COUNT];
};

#define sGameServerSession (*GameServerSession::instance())
