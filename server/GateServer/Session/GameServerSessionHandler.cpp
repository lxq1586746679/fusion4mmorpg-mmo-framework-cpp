#include "preHeader.h"
#include "GameServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GameServerSessionHandler::GameServerSessionHandler()
{
	handlers_[GateServer2GameServer::SGG_REGISTER_RESP] = &GameServerSessionHandler::HandleRegisterResp;
	handlers_[GateServer2GameServer::SGG_ADD_MAP_SERVER] = &GameServerSessionHandler::HandleAddMapServer;
	handlers_[GateServer2GameServer::SGG_REMOVE_MAP_SERVER] = &GameServerSessionHandler::HandleRemoveMapServer;
	handlers_[GateServer2GameServer::SGG_PUSH_SERVER_ID] = &GameServerSessionHandler::HandlePushServerId;
	handlers_[GateServer2GameServer::SGG_PUSH_SOCIAL_LISTEN] = &GameServerSessionHandler::HandlePushSocialListen;
	handlers_[GateServer2GameServer::SGG_PUSH_PACKET_TO_ALL_CLIENT] = &GameServerSessionHandler::HandlePushPacketToAllClient;
	handlers_[GateServer2GameServer::SGG_KICK_ACCOUNT] = &GameServerSessionHandler::HandleKickAccount;
	handlers_[GateServer2GameServer::SGG_KILL_ACCOUNT] = &GameServerSessionHandler::HandleKillAccount;
	handlers_[GateServer2GameServer::SGG_PUSH_CHARACTER] = &GameServerSessionHandler::HandlePushCharacter;
	handlers_[GateServer2GameServer::SGG_PUSH_PLAYER] = &GameServerSessionHandler::HandlePushPlayer;
};

GameServerSessionHandler::~GameServerSessionHandler()
{
}
