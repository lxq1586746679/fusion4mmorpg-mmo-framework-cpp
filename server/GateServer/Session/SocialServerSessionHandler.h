#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class SocialServerSession;

class SocialServerSessionHandler :
	public MessageHandler<SocialServerSessionHandler, SocialServerSession, GateServer2SocialServer::GATE2SOCIAL_MESSAGE_COUNT>,
	public Singleton<SocialServerSessionHandler>
{
public:
	SocialServerSessionHandler();
	virtual ~SocialServerSessionHandler();
private:
	int HandleRegisterResp(SocialServerSession *pSession, INetPacket &pck);
	int HandlePushPacketToAllClient(SocialServerSession *pSession, INetPacket &pck);
};

#define sSocialServerSessionHandler (*SocialServerSessionHandler::instance())
