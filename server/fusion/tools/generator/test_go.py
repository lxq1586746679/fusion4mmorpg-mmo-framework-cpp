import sys
from table_go_generator import *

if __name__ == '__main__':
    cfg = GoTableConfig('worlddb', 'uint', '0')
    for i in range(1, len(sys.argv), 2):
        to_go_files(sys.argv[i], sys.argv[i+1], cfg)
