#pragma once

#include "Thread.h"
#include "Base.h"

class FrameWorker : public Thread
{
public:
    THREAD_RUNTIME(FrameWorker)

    FrameWorker();
    virtual ~FrameWorker();

protected:
    virtual bool Initialize();
    virtual void Kernel();

    virtual void Update(uint64 diffTime) {}
    virtual void OnTick() {}

    virtual void Idle(uint64 ms);

    uint64 time_slice_ms_;
    uint64 time_max_frame_ms_;

    uint64 time_tick_slice_ms_;

private:
    uint64 last_update_time_;
    uint64 total_poll_times_;
    uint64 expired_poll_times_;

    uint64 time_tick_surplus_ms_;
};
