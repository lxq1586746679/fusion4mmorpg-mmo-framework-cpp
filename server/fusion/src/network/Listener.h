#pragma once

#include "Thread.h"
#include "Macro.h"

class ConnectionManager;
class SessionManager;
class Session;

class Listener : public Thread
{
public:
    THREAD_RUNTIME(Listener)

    Listener();
    virtual ~Listener();

    const std::string &addr() const { return addr_; }
    const std::string &port() const { return port_; }

protected:
    virtual bool Prepare();
    virtual bool Initialize();
    virtual void Kernel();
    virtual void Finish();

    virtual bool BindSocketReady(SOCKET sockfd) { return true; }

    virtual std::string GetBindAddress() = 0;
    virtual std::string GetBindPort() = 0;

    virtual Session *NewSessionObject() = 0;
    virtual void AddDataPipes(Session *session) {}

private:
    bool RenewBindPort();
    void OnAcceptComplete(int family, SOCKET sockfd);

    SOCKET sockfd_;

    std::string addr_;
    std::string port_;
};
