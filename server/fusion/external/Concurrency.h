#pragma once

class fakelock {
public:
    void lock() {}
    void unlock() {}
};

#if defined(_WIN32)
    #include <concrt.h>
    class spinlock {
    public:
        void lock() { object_.lock(); }
        bool try_lock() { return object_.try_lock(); }
        void unlock() { object_.unlock(); }
    private:
        concurrency::critical_section object_;
    };
#else
    #include <pthread.h>
    class spinlock {
    public:
        spinlock() { pthread_spin_init(&object_, 0); }
        ~spinlock() { pthread_spin_destroy(&object_); }
        void lock() { pthread_spin_lock(&object_); }
        bool try_lock() { return pthread_spin_trylock(&object_) == 0; }
        void unlock() { pthread_spin_unlock(&object_); }
    private:
        pthread_spinlock_t object_;
    };
#endif
